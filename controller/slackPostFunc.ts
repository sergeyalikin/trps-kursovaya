import dotenv = require('dotenv')
import {IncomingWebhook} from '@slack/webhook'
import {WebClient} from '@slack/web-api'
import {Commit, Builds} from '../types'
import {Gitlab} from 'gitlab'

dotenv.config()
const web = new WebClient(`${process.env.BOT_AUTH}`)
const channelWebhook = new IncomingWebhook(`${process.env.SLACK_WEBHOOK_URL}`)
const api = new Gitlab({
  host:   'https://gitlab.com',
  token: process.env.GITLAB_TOKEN,
})

const loginsMap = new Map([
  ['sergeyalikin', 'UN6V24ZQS'],
  ['dk', 'UM24YL8SC'],
  ['a.shevtsov', 'ULVQHF88Z'],
  ['Kirill1221', 'UNKNF33UY'],
  ['salikin', 'UM25FN9GV'],
  ['alikin.sergey-10', 'sergeyalikin'],
  ['kirilaliki', 'Kirill1221'],
])

export const commentEvents = (fromName: string, url: string, comment: string, eventName: string) => {
  const userArr: string[] = comment.split(' ').filter((elem) => {
    return elem.indexOf('@') === 0
  }).map((elem) => {
    return elem.replace(/@/g, '')
  })
  Array.from(new Set(userArr)).forEach((elem) => {
    if (loginsMap.has(elem)) {
      const slackMessage = {
        channel: `${loginsMap.get(elem)}`,
        as_user: true,
        text: `${fromName} отметил тебя в комментарии к ${eventName} "${comment}".\n${url}`,
      }
      web.chat.postMessage(slackMessage)
      console.log('Message received')
    } else {
      console.log('The user is not in the GitLab project')
    }
  })
}

export const mergeRequestEvents = (name: string, user: string, url: string, action: string) => {
  if ((loginsMap.has(name)) && (action === 'open')) {
    const slackMessage = {
      channel: `${loginsMap.get(name)}`,
      as_user: true,
      text: `${user} назначил на тебя Merge Requests ${url}`,
    }
    web.chat.postMessage(slackMessage)
    console.log('Message received')
  } else {
    console.log('The user is not in the GitLab project or the event is not registered')
  }
}

export const repNewsEvent = (user: string, project: string, branch: string, count: number, commitInf: Commit[]) => {
  branch = branch.slice(branch.lastIndexOf('/') + 1)
  const slackMessage = {
    text: `${user} запушил в проект "${project}" на ветку ${branch} ${count} коммита(ов).`,
    attachments: commitInf.map((elem, i) => ({
      text: `${elem.message}`,
      title: `Коммит №${i + 1}`,
      title_link: elem.url,
      color: '#84c3be',
    })),
  }
  channelWebhook.send(slackMessage)
  console.log('Message received')
}

export const pipelineEvent = (name: string, status: string, builds: Builds[], message: string, url: string) => {
  if (loginsMap.has(name)) {
  const slackMessage = {
    channel: `${loginsMap.get(name)}`,
    as_user: true,
    text: (status === `success`) ? `Pipeline на коммит <${url}|${message.slice(0, -1)}> прошел` : `Pipeline на коммит <${url}|${message.slice(0, -1)}> не прошел`,
    attachments: builds.map((elem) => ({
      title: (elem.status === 'success') ? `:heavy_check_mark: ${elem.name}` : `:X: ${elem.name}`,
      color: (elem.status === 'success') ? '#008000' : '#ff0000',
    })),
  }
  web.chat.postMessage(slackMessage)
  } else {
    console.log('The user is not in the GitLab project or the event is not registered')
  }
  console.log('Success')
}

export const requestForMR = async (text: string, userid: string) => {
  const userArr: string[] = text.split(' ').filter((elem) => {
    return elem.indexOf('@') === 0
  }).map((elem) => {
    return elem.replace(/@/g, '')
  })
  Array.from(new Set(userArr)).forEach(async (elem) => {
    if (!loginsMap.has(elem)) {
      web.chat.postMessage({
        channel: userid,
        as_user: true,
        text: 'Не найден такой пользовотель!',
      })
      return
    }
    const user = await api.Users.all({
      username: loginsMap.get(elem),
    })
    const mr: any = await api.MergeRequests.all({
      state: 'opened',
      assignee_id: user[0].id,
    })
    const slackMessage = {
      channel: userid,
      as_user: true,
      text: `MR на пользователя ${user[0].username}:`,
      attachments: mr.map((element) => ({
        text: `Исполнитель: ${element.assignee.username} \nНазначитель: ${element.author.username}` +
        `\nДата назначения: ${element.created_at.slice(0, 10)}` +
        `\nДата последнего изменения: ${element.updated_at.slice(0, 10)}`,
        title: element.title,
        title_link: element.web_url,
        color: '#FFA500',
      })),
    }
    web.chat.postMessage(slackMessage)
  })
}

export const requestForBranch = async (userid) => {
  const projectId: number = Number(process.env.GITLAB_PROJECT_ID)
  const branchRes: any = await api.Branches.all(projectId)
  const slackMessage = {
    channel: userid,
    as_user: true,
    text: `Ветки проекта:`,
    attachments: branchRes.map((element) => ({
      text: element.title,
      title: element.name,
      color: '#008080',
    })),
  }
  web.chat.postMessage(slackMessage)
}

export const requestComBr = async (userid: string, text: string) => {
  const projectId: number = Number(process.env.GITLAB_PROJECT_ID)
  const branchRes: any = await api.Branches.all(projectId)
  const nameArr: string[] = []
  branchRes.forEach((element) => {
    for (const key in element) {
      if (key === 'name') {
        nameArr.push(element[key])
      }
    }
  })
  const brArr: string[] = text.split(' ').filter((elem) => {
    return elem.indexOf('!') === 0
  }).map((elem) => {
    return elem.replace(/!/g, '')
  })
  const result = brArr.filter((elem) => nameArr.includes(elem))
  if (!result.length) {
    web.chat.postMessage({
      channel: userid,
      as_user: true,
      text: 'Вы не указали существующую ветку или не поставили "!" перед названием ветки!',
    })
    return
  }
  Array.from(new Set(result)).forEach(async (elem) => {
    const commitBrRes: any = await api.Commits.all(projectId, {
      perPage: 5,
      maxPages: 1,
      ref_name: elem,
    })
    const slackMessage = {
      channel: userid,
      as_user: true,
      text: `Последние 5 коммитов на ветке ${elem}`,
      attachments: commitBrRes.map((element, i) => ({
        text: `${element.message}`,
        title: `Коммит №${i + 1}`,
        color: '#84c3be',
      })),
    }
    web.chat.postMessage(slackMessage)
  })
}
