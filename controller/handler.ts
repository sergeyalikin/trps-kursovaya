import {mergeRequestEvents, commentEvents, repNewsEvent, pipelineEvent, requestForMR,
   requestForBranch, requestComBr} from './slackPostFunc'
import express = require('express')
import {JSONFromGitLab} from '../types'
export const handlerFunc = (req: express.Request, res: express.Response) => {
  if ('object_kind' in req.body) {
  const gitLabInf: JSONFromGitLab = req.body
  if (gitLabInf.object_kind === 'merge_request') {
      const gitLabForUsername = gitLabInf.assignees[0].username
      const gitLabFromUsername = gitLabInf.user.username
      const mergeRequestURL = gitLabInf.object_attributes.url
      const mergeRequestAction = gitLabInf.object_attributes.action
      mergeRequestEvents(gitLabForUsername, gitLabFromUsername, mergeRequestURL, mergeRequestAction)
      res.sendStatus(200)
    } else if (gitLabInf.object_kind === 'note') {
      const gitLabComment = gitLabInf.object_attributes.note
      const user = gitLabInf.user.username
      const commentURL = gitLabInf.object_attributes.url
      const noteType = gitLabInf.object_attributes.noteable_type
      commentEvents(user, commentURL, gitLabComment, noteType)
      res.sendStatus(200)
    } else if (gitLabInf.object_kind === 'push') {
      const userName = gitLabInf.user_username
      const gitLabProjectName = gitLabInf.project.name
      const branch = gitLabInf.ref
      const commitCount = gitLabInf.total_commits_count
      const commits = gitLabInf.commits
      repNewsEvent(userName, gitLabProjectName, branch, commitCount, commits)
      res.sendStatus(200)
    } else if (gitLabInf.object_kind === 'pipeline') {
      const user = gitLabInf.user.username
      const status = gitLabInf.object_attributes.status
      const builds = gitLabInf.builds
      const message = gitLabInf.commit.message
      const url = gitLabInf.commit.url
      pipelineEvent(user, status, builds, message, url)
      res.sendStatus(200)
    } else {
      console.log('The event is not registered')
      res.sendStatus(204)
    }
  } else if (req.body.command === '/mrinf') {
    const commandInf = req.body
    requestForMR(commandInf.text, commandInf.user_id)
    res.sendStatus(200)
  } else if (req.body.command === '/brinf') {
    const commandInf = req.body
    requestForBranch(commandInf.user_id)
    res.sendStatus(200)
  } else if (req.body.command === '/combr') {
    const commandInf = req.body
    requestComBr(commandInf.user_id, commandInf.text)
    res.sendStatus(200)
  }
}
