FROM sandrokeil/typescript as builder
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

COPY . .
RUN yarn assembly
RUN yarn install --frozen-lockfile --production

FROM node:10.15.3-alpine

WORKDIR /app

COPY package.json ./

COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app *.js

ENTRYPOINT [ "node", "." ]
