export type Commit = {
    id: string
    message: string
    url: string
    added: string[]
    modified: string[]
    removed: string[]
  }
type Project = {
    name: string
  }
type ObjectAttributes = {
    url: string
    action: string
    note: string
    noteable_type: string
    status: string
  }
type User = {
    username: string
  }
type Assignees = {
    username: string
  }
export type JSONFromGitLab = {
    assignees: Assignees
    object_kind?: string
    user: User
    object_attributes: ObjectAttributes
    user_username: string
    project: Project
    ref: string
    total_commits_count: number
    commits: Commit[]
    commit: PipelineCommit
    builds: Builds[]
  }
export type Builds = {
    name: string
    status: string
 }
type PipelineCommit = {
  message: string
  url: string
}
export type MR = {
  assignee: Assignee
  author: Author
  web_url: string
  title: string
  target_branch: string
  source_branch: string
  created_at: string
  updated_at: string
}
type Assignee = {
  username: string
}
type Author = {
  username: string
}
