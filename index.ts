import express = require('express')
import bodyParser = require('body-parser')
import {handlerFunc} from './controller/handler'
import SlackBot = require ('slackbots')
import dotenv = require('dotenv')
dotenv.config()
const urlencodedParser = bodyParser.urlencoded({
  extended: false,
})
const bot = new SlackBot({
  token: process.env.BOT_AUTH,
  name: 'slacker',
})
bot.on('start', () => {
  console.log('Bot works')
})
const app = express()
app.use(bodyParser.json())
app.post('/', urlencodedParser, handlerFunc)
app.listen(8000)
